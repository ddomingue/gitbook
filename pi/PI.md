## Using CEC to control monitor from bash

Package installation : sudo apt-get install cec-utils

* Turn monitor on : echo on 0 | cec-client -s -d 1
* Turn monitor off : echo standby 0 | cec-client -s -d 1
* Get monitor status : echo pow 0 | cec-client -s -d 1

Useful CEC translation site with exhaustive list of functionalities [CEC-O-MATIC](http://www.cec-o-matic.com/)


## Controlling GPIO from bash

Package installation : sudo apt-get install rpi.gpio

* Read gpio info : gpio readall
* Set pin gpio mode : gpio mode {pin} in/out/pwm/clock/up/down/tri
* Read gpio on mode in : gpio read {pin}
* Write to gpio on mode out : gpio write {pin} 0/1

## Detecting device on lan using mac address

Package installation : sudo apt-get install arp-scan

* sudo arp-scan -l --dest-addr={mac_address} --interface={if_interface} -r {num_retries}


## Sending SMS through an API

* International provider [textbelt](https://textbelt.com/)
* SMS Notification API from free.fr (must have free mobile account, messages are sent to account number only) 

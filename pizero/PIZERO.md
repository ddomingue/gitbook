## PI Zero Installation and configuration

### Components

* Raspberry Pi Zero v1.3
* 8Gb Micro SD Card
* USB to MicroUSB Adapter
* HDMI to MicroHDMI Adapter
* Micro usb power cable

### Installing the OS (Raspbian Lite)

* [Raspbian lite](https://www.raspberrypi.org/downloads/raspbian/)
* Installed using [Etcher](https://etcher.io/) 